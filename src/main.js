import React from "react";
import { Container } from 'react-materialize';
import { Switch, Route } from 'react-router-dom'
import Home from "./components/home/home";
import Contact from "./components/contact/contact";
import NoMatch from "./components/noMatch/noMatch";

const Main = () => (
    <main>
        <Container>
            <Switch>
                <Route exact path='/' component={Home}/>
                <Route path='/contact' component={Contact}/>
                <Route component={NoMatch}/>
            </Switch>
        </Container>
    </main>  
);

export default Main;