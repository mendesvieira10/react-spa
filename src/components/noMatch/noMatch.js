import React from "react";
import { Row, Col, Icon,  Button } from 'react-materialize';
import { Route } from 'react-router-dom'

const NoMatch = () => (
  <Row>
    <Col m={8} s={12}>
        <h5>Sorry</h5>
        <Route render={({ history}) => (
            <Button waves='light'  
                            className="purple darken-2"
                            onClick={() => { history.push('/') }}>
                Back to  home
                <Icon left>arrow_back</Icon>
            </Button>
        )} />
    </Col>
  </Row>
);

export default NoMatch;