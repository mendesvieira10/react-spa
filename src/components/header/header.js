import React from "react";
import { Navbar, NavItem, Row, Col, Dropdown } from 'react-materialize';
import { NavLink } from 'react-router-dom'
import avatar from '../../images/avatar.png';
import style from './header.css';


const Header = () => (
	<Row>
		<Navbar className="purple darken-2" brand='Portifolio' right>
			<li>
				<NavLink to="/">Home
				</NavLink>
			</li>

			<li>
				<NavLink to="contact">Contact
				</NavLink>
			</li>
			
			<li>
				<NavItem>
					<Dropdown className="dropdown-fix" trigger={
								
					
						<Col> R$ 500,00 
						
							<Col>
								<img className="circle responsive-img nav-img"
									height={37} 
									width={37} 
									src={avatar} />
							</Col>
						</Col>
						}>
						<NavItem>one</NavItem>
						<NavItem>two</NavItem>
						<NavItem divider />
						<NavItem>three</NavItem>
					</Dropdown>

				</NavItem>
			</li>
		</Navbar>
	</Row>
);

export default Header;